import { useEffect, useState } from "react";
import "./App.css";
import ButtonComp from "./components/button";
import { useSelector } from "react-redux";

function App() {
  const { value } = useSelector((state) => state.counter);

  return (
    <div>
      <p>You clicksed {value} times</p>
      <ButtonComp />
    </div>
  );
}

export default App;
