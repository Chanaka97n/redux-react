import React from "react";
import { Button } from "antd";
import { useDispatch } from "react-redux";
import { decrement, increment } from "../redux/features/counter/counterSlice";

function ButtonComp() {
  const dispatch = useDispatch();

  return (
    <div className="">
      {" "}
      <Button onClick={() => dispatch(increment())}>+</Button>
      <Button onClick={() => dispatch(decrement())}>-</Button>
      {/* <Button type="primary">Button</Button> */}
    </div>
  );
}

export default ButtonComp;
